The Autosuggest module provides suggestions for autocomplete entity reference
fields chosen from previous values. It is 100% client-side; if you want to see
how it works, look in autosuggest.js. Here's how it works:

 - When the page is loaded, suggestions for relevant fields are pulled out of
   localStorage and displayed below the field.
 - When a suggestion is clicked, it replaces the field's value.
 - When a field loses focus, such as when a user finishes typing into it or
   selects on autocomplete value, the field's value is stored in localStorage
   so that it can be retrieved on the next page load.

It's that simple -- no database or form-handling trickery required. Obviously
JavaScript must be enabled for the module to do its job, but if JavaScript is
disabled then autocomplete won't work in the first place.

Written by Isaac Sukin (IceCreamYou) for The Module Off.
http://www.isaacsukin.com/