(function ($) {
  Drupal.behaviors.autosuggest = {
    attach: function (context) {
      // When an autocomplete field is unfocused, store its value.
      $('.field-type-entityreference .form-autocomplete', context).blur(function() {
        var l = $.parseJSON(localStorage.getItem('autosuggest')), $t = $(this), v = $t.val();
        // If we already have values stored, add the new value to the list.
        // There's obviously much more you could do here:
        // - Store how often each term is chosen and show the most popular ones
        //   (possibly even most popular within the last N days)
        // - Store values per field (use the "name" attribute of the field)
        // - Make the maximum number of values to show into a module setting
        // - Show the date the item was chosen in the title text
        // These are all easy, but I'm out of time and patience.
        if (l instanceof Array && $.inArray(v, l) === -1) {
          l.unshift(v);
          // Don't show more than a certain number of values.
          if (l.length > 5) {
            l.pop();
          }
        }
        // If we don't have any values stored, create a new list.
        else {
          l = [v];
        }
        // localStorage only holds strings, so encode the array as JSON.
        localStorage.setItem('autosuggest', JSON.stringify(l));
      }).each(function(i, v) {
        var l = $.parseJSON(localStorage.getItem('autosuggest'));
        if (!(l instanceof Array)) {
          return;
        }
        // Build and print the list of suggestions.
        // TODO: Add a field setting to enable or disable this behavior.
        var output = '<div class="autosuggest">';
        for (var i = 0, n = l.length; i < n; i++) {
          var safe = $('<div>').text(l[i]).html(); // Sanitize the suggestion
          output += '<span class="autosuggest-item">' + safe + '</span>';
        }
        output += '</div>';
        $(v).after(output);
      });
      // When a suggestion is clicked, fill in the nearest autocomplete field.
      $('.autosuggest-item', context).click(function(e) {
        e.preventDefault();
        $(this).closest('.field-type-entityreference').find('.form-autocomplete').val($(this).text());
      });
    }
  }
})(jQuery);
